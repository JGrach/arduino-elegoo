#ifndef MOTOR_H
#define MOTOR_H

#include <Arduino.h>

#define MOTOR_MODE_DIGITAL 0
#define MOTOR_MODE_MANUAL 1

#define MOTOR_STATE_BACKWARD -1
#define MOTOR_STATE_STOP 0
#define MOTOR_STATE_FORWARD 1

#define MAX_SPEED 255
#define MIN_SPEED 0

class Motor {
    public: 
        Motor(int pinPwm, int pinForward, int pinBackward);
        void forward();
        void forward(int speed);
        void stop();
        void backward();
        void backward(int speed);
        void setSpeed(int speed);
        int getSpeed();
    private:
        int _pinPwm;
        int _pinForward;
        int _pinBackward;
        int _speed;
        int _state;
        int _mode;
        void updateDigital(uint8_t speed);
        void updateManual(int speed);
};

#endif // MOTOR_H
