#include <Arduino.h>
#include "Motor.h"

Motor::Motor(int pinPwm, int pinForward, int pinBackward) {
    _pinPwm = pinPwm;
    pinMode(_pinPwm,OUTPUT);

    _pinBackward = pinBackward;
    pinMode(pinBackward,OUTPUT);

    _pinForward = pinForward;
    pinMode(pinForward,OUTPUT);

    _speed = 0;
    _state = MOTOR_STATE_STOP;
    _mode = MOTOR_MODE_DIGITAL;
};

void Motor::updateDigital(uint8_t speed) {
    digitalWrite(_pinPwm, speed);
    _speed = speed == HIGH ? MAX_SPEED : MIN_SPEED;
    _mode = MOTOR_MODE_DIGITAL;
};

void Motor::updateManual(int speed) {
    if (speed != _speed) {
        analogWrite(_pinPwm, speed);
        _speed = speed;
        _mode = MOTOR_MODE_MANUAL;
    }
};

void Motor::forward() {
    digitalWrite(_pinForward, HIGH);
    digitalWrite(_pinBackward, LOW);
    _state = MOTOR_STATE_FORWARD;
    this->updateDigital(HIGH);
};

void Motor::forward(int speed) {
    if (_state != MOTOR_STATE_FORWARD) {
        digitalWrite(_pinForward, HIGH);
        digitalWrite(_pinBackward, LOW);
        _state = MOTOR_STATE_FORWARD;
    }
    this->updateManual(speed);
};

void Motor::backward() {
    digitalWrite(_pinForward, LOW);
    digitalWrite(_pinBackward, HIGH);
    _state = MOTOR_STATE_BACKWARD;
    this->updateDigital(HIGH);
};

void Motor::backward(int speed) {
    if (_state != MOTOR_STATE_BACKWARD) {
        digitalWrite(_pinForward, LOW);
        digitalWrite(_pinBackward, HIGH);
        _state = MOTOR_STATE_BACKWARD;
    }
    this->updateManual(speed);
};

void Motor::stop() {
    digitalWrite(_pinForward, LOW);
    digitalWrite(_pinBackward, LOW);
    digitalWrite(_pinPwm, LOW);
    _state = MOTOR_STATE_STOP;
    this->updateDigital(LOW);
};

void Motor::setSpeed(int speed) {
    if (speed > 0) this->forward(speed);
    else if (speed < 0) this->backward(-speed);
    else this->stop();
};

int Motor::getSpeed() {
    return _speed;
};

