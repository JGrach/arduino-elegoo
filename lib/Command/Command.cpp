#include <Arduino.h>
#include "Command.h"

Command::Command() {
    _infos = NULL;
    _infosLength = NULL;
}

void Command::attach(Info *infos, int length) {
    _infos = infos;
    _infosLength = length;
}

void Command::onEvent(String inputString) {
    int flagParser = inputString.indexOf(FLAG_PARSER);

    String flag = flagParser < 0 ? FLAG_MISSING : inputString.substring(0, flagParser);
    String message = flagParser < 0 ? inputString : inputString.substring(flagParser + String(FLAG_PARSER).length());

    this->fire(flag, message);
}

void Command::fire(String flag, String message) {
    for(int i = 0; i < _infosLength; i++) {
        if (_infos[i].flag == flag) {
            _infos[i].function(message);
        }
    }
}