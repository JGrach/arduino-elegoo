#ifndef COMMAND_H
#define COMMAND_H

#include <Arduino.h>

#define FLAG_PARSER "->"
#define FLAG_MISSING "MISS_FLAG"

struct Info {
    String flag;
    void (* function)(String message);
};

class Command {
    public:
        Command();
        void attach(Info *infos, int length);
        void onEvent(String inputString);
    private: 
        Info *_infos;
        int _infosLength;
        void fire(String flag, String message);
};

#endif // COMMAND_H
