#ifndef SONAR_H
#define SONAR_H

#include <Arduino.h>
#include <Servo.h>

#define SONAR_MINIMAL_ANGLE_MS 700
#define SONAR_MAXIMAL_ANGLE_MS 2400
#define SONAR_DEFAULT_ANGLE 90

#define SONAR_TIME_TO_CLEAN_WAVE 2
#define SONAR_WAVE_TIME 10

#define SONAR_SOUND_SPEED_CM 0.0343
#define SONAR_TIME_TO_DISTANCE(time) ((time / 2) * SONAR_SOUND_SPEED_CM)

struct Obstacle {
    int angle;
    int distance;
};

class Sonar {
    public: 
        Sonar();
        Sonar::Sonar(int pinTrig, int pinRecept, int pinServo);
        void attach(int pinTrig, int pinRecept, int pinServo);
        Obstacle getObstacle();
        void setAngle(int angle);
    private:
        int _pinTrig;
        int _pinRecept;
        int _angle;
        Servo servoSonar;
};

#endif // SONAR_H
