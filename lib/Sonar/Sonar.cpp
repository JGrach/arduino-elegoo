#include <Arduino.h>
#include "Sonar.h"

Sonar::Sonar() {}

Sonar::Sonar(int pinTrig, int pinRecept, int pinServo) {
    this->attach(pinTrig, pinRecept, pinServo);
}

void Sonar::attach(int pinTrig, int pinRecept, int pinServo) {
    _pinTrig = pinTrig;
    pinMode(_pinTrig, OUTPUT);  

    _pinRecept = pinRecept;
    pinMode(_pinRecept, INPUT);    

    servoSonar.attach(pinServo);
    this->setAngle(SONAR_DEFAULT_ANGLE);
};

Obstacle Sonar::getObstacle() {
    digitalWrite(_pinTrig, LOW);   
    delayMicroseconds(SONAR_TIME_TO_CLEAN_WAVE);
    digitalWrite(_pinTrig, HIGH);  
    delayMicroseconds(SONAR_WAVE_TIME);
    digitalWrite(_pinTrig, LOW);   
    int Fdistance = pulseIn(_pinRecept, HIGH);
    return {
        _angle,
        SONAR_TIME_TO_DISTANCE(Fdistance)
    };
};

void Sonar::setAngle(int angle) {
    _angle = angle;
    servoSonar.write(angle);
}


