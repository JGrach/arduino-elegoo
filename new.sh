#!/bin/bash

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
MAKEFILE_PATH="$PROJECT_DIR/Makefile-Linux.mk"

# CONDITION ON PROJECT ALREADY EXIST

if [[ -n $(find $PROJECT_DIR/src -name $1) ]] 
then 
    echo "Error: $PROJECT_DIR/src/$1 already exist"
    exit 1
fi

# CREATE PROJECT

SRC_DIR="$PROJECT_DIR/src/$1"

mkdir $SRC_DIR
cp $MAKEFILE_PATH "$SRC_DIR/Makefile"
echo $'#include <Arduino.h>\n\nvoid setup() {\n}\n\nvoid loop() {\n}' >> "$SRC_DIR/main.cpp"

# CHANGE MAKEFILE PROJECT_DIR

OLD_PROJECT_DIR=$(grep -E 'PROJECT_DIR\s+=\s+' "$SRC_DIR/Makefile")
NEW_PROJECT_DIR="PROJECT_DIR = $PROJECT_DIR"

sed -i "s/$OLD_PROJECT_DIR/${NEW_PROJECT_DIR//\//\\/}/g" "$SRC_DIR/Makefile"


