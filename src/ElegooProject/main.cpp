#include <Arduino.h>
#include <Servo.h>
#include "Motor.h"
#include "Command.h"
#include "Sonar.h"

#define ENA 5
#define ENB 6
#define IN1 7
#define IN2 8
#define IN3 9
#define IN4 11

String inputString = "";

Motor motorRight = Motor(ENB, IN4, IN3);
Motor motorLeft = Motor(ENA, IN1, IN2);

Sonar sonar;
Command actioner;

const int actionerListenersLength = 4;
Info actionerListeners[actionerListenersLength] = {
    {"SERIAL_CALL", [](String message) { Serial.print(message); } },
    {"RIGHT_SPEED", [](String message) { motorRight.setSpeed(message.toInt()); } }, 
    {"LEFT_SPEED", [](String message) { motorLeft.setSpeed(message.toInt()); } }, 
    {"SONAR_ANGLE", [](String message) { sonar.setAngle(message.toInt()); } }, 
};

void setup() {
    Serial.begin(9600);

    sonar.attach(A5, A4, 3);

    actioner.attach(actionerListeners, actionerListenersLength);
}

void loop() {
    Obstacle obstacle = sonar.getObstacle();
    actioner.onEvent("SERIAL_CALL->ang:" + String(obstacle.angle) + "/dist:" + String(obstacle.distance));
    delay(1000);
}

void serialEvent() {
    while (Serial.available()) {
        String inputString = Serial.readStringUntil(';');
        actioner.onEvent(inputString);
    }
}